
/*
  TODO
    write label on biggest node per category
    better color scale
    responsive labels
*/

import React, { Component } from 'react'
import data from './data.csv'
import { csv } from 'd3-request'
import { color } from 'd3-color'
import { hierarchy, treemap } from 'd3-hierarchy'
import './styles.css'

export default class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      dataReady: false,
      tree: null,
      layout: null,
      fadedNodes: {},
      disabledFilters: {},
      disabledNodes: {}
    }

    this.updateFadedNodes = this.updateFadedNodes.bind(this)
    this.updateFilters = this.updateFilters.bind(this)
    this.updateLayout = this.updateLayout.bind(this)
  }

  componentDidMount () {
    const {
      width,
      height
    } = this.refs.viz.getBoundingClientRect()

    csv(data, (error, data) => {
      if (error) {
        throw error
      }

      const dataTree = {
        name: 'ok',
        children: []
      }

      data
        .filter(row => {
          return row && row.date
        })
        .map(datum => {
          datum.duration = parseInt(datum.duration)
          return datum
        })
        .forEach((datum, i) => {
          const treeLocation = datum.provenance.split('/').concat([`content-${i}`])
          let leaf = dataTree
          treeLocation.forEach((node, j, a) => {
            let newLeaf = leaf.children.find(n => node === n.name)

            if (!newLeaf) {
              let color = '#220000'
              if (datum.provenance.indexOf('new/subscription') > -1) color = '#ffaf28'
              else if (datum.provenance.indexOf('new/recommendation/irl') > -1) color = '#cc2211'
              else if (datum.provenance.indexOf('new/recommendation/follow') > -1) color = '#ff7700'
              else if (datum.provenance.indexOf('new/recommendation/algorithm') > -1) color = '#ee4455'
              else if (datum.provenance.indexOf('new/recommendation/ad') > -1) color = '#aa5500'
              else if (datum.provenance.indexOf('new/search') > -1) color = '#ff8888'
              else if (datum.provenance.indexOf('new/link') > -1) color = '#cc2211'
              else if (datum.provenance.indexOf('old/owned') > -1) color = '#0088ff'
              else if (datum.provenance.indexOf('old/url') > -1) color = '#00aacc'
              else if (datum.provenance.indexOf('old/bookmarks') > -1) color = '#44cc11'
              else if (datum.provenance.indexOf('old/search') > -1) color = '#6611dd'
              else console.log(datum.provenance)

              let label = ''
              if (datum.provenance.indexOf('new/subscription') > -1) label = 'subscription'
              else if (datum.provenance.indexOf('new/recommendation/irl') > -1) label = 'recommended by friends'
              else if (datum.provenance.indexOf('new/recommendation/follow') > -1) label = 'recommended by network'
              else if (datum.provenance.indexOf('new/recommendation/algorithm') > -1) label = 'recommended by algorithm'
              else if (datum.provenance.indexOf('new/recommendation/ad') > -1) label = 'ads'
              else if (datum.provenance.indexOf('new/search') > -1) label = 'new search'
              else if (datum.provenance.indexOf('new/link') > -1) label = 'link'
              else if (datum.provenance.indexOf('old/owned') > -1) label = 'ownership'
              else if (datum.provenance.indexOf('old/url') > -1) label = 'memory'
              else if (datum.provenance.indexOf('old/bookmarks') > -1) label = 'bookmarks'
              else if (datum.provenance.indexOf('old/search') > -1) label = 'old search'

              let time = parseInt(datum.date.split('/')[1]) / 100
              time = time >= 6 && time < 12 ? 'morning'
                : time >= 12 && time < 19 ? 'afternoon'
                  : 'night'

              const device = datum.device !== 'phone' && datum.device !== 'laptop' ? 'others' : datum.device

              const shade = Math.random() / 2

              newLeaf = {
                name: node,
                id: a[j - 1],
                description: datum.description,
                color,
                label,
                children: [],
                value: 0,
                duration: 0,
                count: 0,
                type: datum.type,
                device,
                time,
                shade,
                discovered: datum.discovered
              }

              leaf.children.push(newLeaf)
            }

            leaf = newLeaf
          })

          leaf.duration = datum.duration
          leaf.count = 1
          leaf.value = leaf.duration
        })

      const dataHierarchy = hierarchy(dataTree)
        .sum(d => d.value)
        .sort((a, b) => b.value - a.value)

      const layout = treemap().size([width, height])(dataHierarchy)

      const types = Object
        .values(
          layout
            .descendants()
            .filter(node => node.data.type && (!node.children || !node.children.length))
            .reduce((map, node) => {
              if (!map[node.data.type]) {
                map[node.data.type] = {
                  name: node.data.type,
                  active: true,
                  count: 0
                }
              }

              map[node.data.type].count += node.value
              return map
            }, {})
        )
        .sort((a, b) => b.count - a.count)

      const devices = Object
        .values(
          layout
            .descendants()
            .filter(node => node.data.device && (!node.children || !node.children.length))
            .reduce((map, node) => {
              if (!map[node.data.device]) {
                map[node.data.device] = {
                  name: node.data.device,
                  active: true,
                  count: 0
                }
              }

              map[node.data.device].count += node.value
              return map
            }, {})
        )
        .sort((a, b) => b.count - a.count)

      const times = Object
        .values(
          layout
            .descendants()
            .filter(node => node.data.time && (!node.children || !node.children.length))
            .reduce((map, node) => {
              if (!map[node.data.time]) {
                map[node.data.time] = {
                  name: node.data.time,
                  active: true,
                  count: 0
                }
              }

              map[node.data.time].count += node.value
              return map
            }, {})
        )
        .sort((a, b) => b.count - a.count)

      this.setState({
        ...this.state,
        width,
        height,
        dataHierarchy,
        layout,
        types,
        devices,
        times
      })
    })
  }

  updateFadedNodes (type, value) {
    const fadedNodes = {}

    if (this.state.disabledFilters[value]) {
      return null
    }

    if (type === 'reset') {
      return this.setState({
        ...this.state,
        fadedNodes
      })
    }

    const {
      layout
    } = this.state

    layout
      .descendants()
      .filter(node => node.data[type] !== value)
      .forEach(node => {
        fadedNodes[node.data.name] = true
      })

    this.setState({
      ...this.state,
      fadedNodes
    })
  }

  updateFilters (type, value) {
    const {
      layout,
      disabledFilters
    } = this.state

    if (!disabledFilters[value]) {
      disabledFilters[value] = true
    } else {
      delete disabledFilters[value]
    }

    const disabledNodes = {}

    layout
      .descendants()
      .filter(node => disabledFilters[node.data.type] || disabledFilters[node.data.device] || disabledFilters[node.data.time])
      .forEach(node => {
        disabledNodes[node.data.name] = true
      })

    // console.log(disabledNodes)
    this.updateLayout(disabledNodes, (layout) => {
      this.setState({
        ...this.state,
        disabledNodes,
        fadedNodes: {},
        layout
      })
    })
  }

  updateLayout (disabledNodes = {}, callback) {
    const {
      width,
      height,
      dataHierarchy
    } = this.state

    dataHierarchy
      .sum(d => !disabledNodes[d.name] ? d.duration : 0)
      .sort((a, b) => b.value - a.value)

    const layout = treemap().size([width, height])(dataHierarchy)

    if (callback) {
      callback(layout)
    } else {
      this.setState({
        ...this.state,
        layout
      })
    }
  }

  setHighlights (node) {
    this.setState({
      ...this.state,
      highlightedNode: node
    })
  }

  render () {
    const {
      layout,
      types,
      devices,
      times,
      fadedNodes,
      disabledFilters,
      highlightedNode
    } = this.state

    // if (!data) {
    //   return null
    // }

    const usedIds = {}

    const viz = !layout ? null
      : layout.descendants()
        .filter(node => !node.children || !node.children.length)
        .map((node, i) => {
          const c = color(node.data.color).rgb().brighter(node.data.shade)

          // const color = cubehelix(Math.random() * 360, 1.3, 0.5).rgb()
          const x = node.x0
          const y = node.y0
          const width = node.x1 - node.x0
          const height = node.y1 - node.y0

          if (height > 20 && width > 10) {
            if (!usedIds[node.data.label]) {
              usedIds[node.data.label] = 0
            }
            usedIds[node.data.label]++
          }

          return (
            <div
              className='node'
              style={{
                width,
                height,
                position: 'absolute',
                transform: `translate(${x}px, ${y}px)`,
                zIndex: usedIds[node.data.label] === 1 ? 1000 - i : 0,
                transition: 'width 0.25s, height 0.25s, transform 0.25s'
              }}
              key={`node-${i}`}
              onMouseOver={() => this.setHighlights(node)}
              onMouseOut={() => this.setHighlights()}
            >
              <div
                style={{
                  position: 'relative',
                  backgroundColor: !fadedNodes[node.data.name] ? c.formatRgb() : c.darker(2).formatRgb(),
                  border: `1px solid ${c.darker(!fadedNodes[node.data.name] ? 1 : 3).formatRgb()}`,
                  width: '100%',
                  height: '100%',
                  transition: 'background-color 0.15s, border 0.15s'
                }}
              >
                {
                  usedIds[node.data.label] === 1 && height > 20 && width > 10 &&
                  <div
                    style={{
                      color: `${c.darker(4).formatRgb()}`,
                      fontSize: Math.max(11, window.innerHeight * 0.014),
                      margin: 5
                    }}
                  >
                    { node.data.label }
                  </div>
                }
              </div>
            </div>
          )
        })

    const legendWidth = !layout ? null
      : layout.descendants()
        .filter(node => node.value === 1)
        .slice(0, 1)
        .reduce((area, node) => Math.sqrt((node.x1 - node.x0) * (node.y1 - node.y0)), 0)

    // const totalDuration = node.value
    const totalDuration = !layout ? null
      : `${Math.floor(layout.value / 60 / 24)} day, ${Math.floor((layout.value % (60 * 24)) / 60)} hours, and ${layout.value % 60} minutes of media consumption, broken down by provenance. `

    const typeFilters = !types ? null
      : types.map((type, i) => {
        return (
          <li
            key={`filters-type-${i}`}
            style={{
              display: 'inline-block',
              cursor: 'pointer',
              padding: '0 5px 5px 0',
              transition: 'opacity 0.15s',
              opacity: !disabledFilters[type.name] ? 1 : 0.4
            }}
            onMouseOver={() => this.updateFadedNodes('type', type.name)}
            onMouseOut={() => this.updateFadedNodes('reset')}
            onClick={() => this.updateFilters('type', type.name)}
          >
            <div
              style={{
                backgroundColor: type.active ? '#999' : '#444',
                padding: '4px 10px',
                color: 'black',
                fontSize: Math.max(11, window.innerHeight * 0.014),
                borderRadius: 2
              }}
            >
              {type.name}
            </div>
          </li>
        )
      })

    const deviceFilters = !devices ? null
      : devices.map((device, i) => {
        return (
          <li
            key={`filters-device-${i}`}
            style={{
              display: 'inline-block',
              cursor: 'pointer',
              padding: '0 5px 5px 0',
              transition: 'opacity 0.15s',
              opacity: !disabledFilters[device.name] ? 1 : 0.4
            }}
            onMouseOver={() => this.updateFadedNodes('device', device.name)}
            onMouseOut={() => this.updateFadedNodes('reset')}
            onClick={() => this.updateFilters('device', device.name)}
          >
            <div
              style={{
                backgroundColor: device.active ? '#999' : '#444',
                padding: '4px 10px',
                color: 'black',
                fontSize: Math.max(11, window.innerHeight * 0.014),
                borderRadius: 2
              }}
            >
              {device.name}
            </div>
          </li>
        )
      })

    const timeFilters = !times ? null
      : times.map((time, i) => {
        return (
          <li
            key={`filters-time-${i}`}
            style={{
              display: 'inline-block',
              cursor: 'pointer',
              padding: '0 5px 5px 0',
              transition: 'opacity 0.15s',
              opacity: !disabledFilters[time.name] ? 1 : 0.4
            }}
            onMouseOver={() => this.updateFadedNodes('time', time.name)}
            onMouseOut={() => this.updateFadedNodes('reset')}
            onClick={() => this.updateFilters('time', time.name)}
          >
            <div
              style={{
                backgroundColor: time.active ? '#999' : '#444',
                padding: '4px 10px',
                color: 'black',
                fontSize: Math.max(11, window.innerHeight * 0.014),
                borderRadius: 2
              }}
            >
              {time.name}
            </div>
          </li>
        )
      })

    const highlights = !highlightedNode ? null
      : [highlightedNode].map(node => {
        let h = Math.floor(node.value / 60)
        let m = node.value % 60
        const timeString = `${h < 10 ? '0' + h : h}:${m < 10 ? '0' + m : m}:00`

        return (
          <div>
            {node.data.description}
            <br />
            {node.data.label}
            <br />
            {`${node.data.type}, ${node.data.discovered}`}
            <br />
            {timeString}
          </div>
        )
      })

    return (
      <div id='app'>
        <h1>
          Media Provenance
        </h1>
        <h2
          style={{
            marginBottom: 50
          }}
        >
          {totalDuration}
          Rectangle size maps to time.
        </h2>
        <div
          style={{
            display: 'flex'
          }}
        >
          <div
            id='viz-wrapper'
            style={{
              flexGrow: 1
            }}
          >
            <div
              id='viz'
              ref='viz'
            >
              {viz}
            </div>
            <div
              className='legend'
              style={{
                display: 'flex',
                marginTop: 20,
                alignItems: 'center'
              }}
            >
              <div
                style={{
                  width: legendWidth,
                  height: legendWidth,
                  border: '1px solid #ccc',
                  borderRadius: 2,
                  marginRight: 8
                }}
              />
              <div
                style={{
                  color: '#aaa',
                  fontSize: Math.max(11, window.innerHeight * 0.014),
                  marginRight: 20
                }}
              >
                = 1 minute
              </div>
              <div
                style={{
                  width: 19,
                  height: 19,
                  display: 'flex',
                  borderRadius: 2,
                  marginRight: 8
                }}
              >
                <div
                  style={{
                    height: '100%',
                    flex: 1,
                    backgroundColor: '#ffaf28'
                  }}
                />
                <div
                  style={{
                    height: '100%',
                    flex: 1,
                    backgroundColor: '#ff7700'
                  }}
                />
                <div
                  style={{
                    height: '100%',
                    flex: 1,
                    backgroundColor: '#cc2211'
                  }}
                />
              </div>
              <div
                style={{
                  color: '#aaa',
                  fontSize: Math.max(11, window.innerHeight * 0.014),
                  marginRight: 20
                }}
              >
                New media
              </div>
              <div
                style={{
                  width: 19,
                  height: 19,
                  display: 'flex',
                  borderRadius: 2,
                  marginRight: 8
                }}
              >
                <div
                  style={{
                    height: '100%',
                    flex: 1,
                    backgroundColor: '#0088ff'
                  }}
                />
                <div
                  style={{
                    height: '100%',
                    flex: 1,
                    backgroundColor: '#44cc11'
                  }}
                />
                <div
                  style={{
                    height: '100%',
                    flex: 1,
                    backgroundColor: '#6611dd'
                  }}
                />
              </div>
              <div
                style={{
                  color: '#aaa',
                  fontSize: Math.max(11, window.innerHeight * 0.014),
                  marginRight: 20
                }}
              >
                Old media
              </div>
            </div>
          </div>
          <div
            id='controls'
            style={{
              width: '20vw',
              paddingLeft: '2vw'
            }}
          >
            <h3>
              Content type
            </h3>
            <ul>
              {typeFilters}
            </ul>
            <h3>
              Device used
            </h3>
            <ul>
              {deviceFilters}
            </ul>
            <h3>
              Time of the day
            </h3>
            <ul>
              {timeFilters}
            </ul>
            <div
              id='credits'
            >
              <a href='https://ian.eart' target='_blank' rel='noopener noreferrer'>By Ian Ardouin-Fumat</a>
              <br />
              <a href='https://fixingsocialmedia.mit.edu/' target='_blank' rel='noopener noreferrer'>For Ethan Zuckerman's class: Fixing Social Media</a>
              <br />
              <a href='https://gitlab.com/iaaaan/media-provenance' target='_blank' rel='noopener noreferrer'>Source code</a>
            </div>
            <div
              id='highlights'
            >
              {highlights}
            </div>
          </div>
        </div>
        <div id='explainer'>
          <p>
            Over the course of seven days, I recorded a data point each time I engaged with published content for ~10 seconds or more. This visualization groups those many pieces of content based on their provenance (e.g content that I deliberately access versus content recommended by algorithms). Each piece of media is represented by a rectangle, and its size is based on the time I spent with it. The resulting visualization shows the footprint of various access channels on my media consumption.
          </p>
          <p>
            As it turns out, my media diet is mostly composed of content whose provenance I control: physical media I own, Youtube channels I subscribe to, and chronologically-sorted Twitter timeline. My interaction with recommendation algorithms and ads is limited to my music consumption: Spotify is almost the only space where I let the machine dictate what comes next. Hypertext links used to be a driving-factor in content discovery; here they're only responsible for a tiny fraction of my consumption.
          </p>
          <p>
            What doesn’t appear here is the many hours spent scrolling aimlessly on Twitter (chronological) and Instagram (algorithm-curated), as I look, but barely engage with the conversation.
          </p>
        </div>
      </div>
    )
  }
}
